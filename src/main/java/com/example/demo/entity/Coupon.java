package com.example.demo.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Coupon {

    private int id;
    private String name;
    private int type;
    private String createdBy;
    private String createdDate;
}
