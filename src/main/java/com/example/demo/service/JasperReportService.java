package com.example.demo.service;

import com.example.demo.entity.Coupon;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class JasperReportService {
    public byte[] getCouponReport(List<Coupon> items, String format) {
        JasperReport jasperReport;
        try {
            File file = ResourceUtils.getFile("APP/coupon-report.jasper");
//            InputStream inputStream = getClass().getResourceAsStream("coupon-report.jasper");
            jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        } catch (JRException e) {
            try {
//                File file = ResourceUtils.getFile("classpath:reports/coupon-report.jrxml");
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream("reports/coupon-report.jrxml");
                jasperReport = JasperCompileManager.compileReport(inputStream);
                JRSaver.saveObject(jasperReport, "coupon-report.jasper");
            } catch (JRException ex) {
                throw new RuntimeException(e);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(items);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", "Coupon Report");
        JasperPrint jasperPrint = null;
        byte[] reportContent;

        try {
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
            switch (format) {
                case "pdf" -> reportContent = JasperExportManager.exportReportToPdf(jasperPrint);
                case "xml" -> reportContent = JasperExportManager.exportReportToXml(jasperPrint).getBytes();
                default -> throw new RuntimeException("Unknown report format");
            }
        } catch (JRException e) {
            throw new RuntimeException(e);
        }
        return reportContent;
    }

}
