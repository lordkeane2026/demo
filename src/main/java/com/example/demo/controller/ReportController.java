package com.example.demo.controller;

import com.example.demo.entity.Coupon;
import com.example.demo.service.JasperReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ReportController {
    @Autowired
    JasperReportService jasperReportService;
    @GetMapping("coupon-report/{format}")
    public ResponseEntity<Resource> getCouponReport(@PathVariable String format) {

        byte[] reportContent = jasperReportService.getCouponReport(getCouponList(), format);

        ByteArrayResource resource = new ByteArrayResource(reportContent);
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(resource.contentLength())
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        ContentDisposition.attachment()
                                .filename("coupon-report." + format)
                                .build().toString())
                .body(resource);
    }

    private List<Coupon> getCouponList(){
        List<Coupon> couponList = new ArrayList<>();

        Coupon coupon = Coupon.builder()
                .id(1)
                .name("test")
                .type(2)
                .createdBy("Tony")
                .createdDate("2023-10-17")
                .build();

        couponList.add(coupon);

        return couponList;
    }
}
